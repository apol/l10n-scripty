
KDE_PROJECTS_API_URL="https://projects.kde.org/api/v1"

I18N_BRANCH='stableKF5'

function check_kde_projects_requirements
{
  local programs_required="curl jq"
  for program_checked in ${programs_required}; do
    ${program_checked} --help &>/dev/null
    if [ $? -ne 0 ]; then
      printf "\'${program_checked}\' is required but it was not found\n" 1>&2
      exit 1
    fi
  done
}

function list_modules
{
  check_kde_projects_requirements

  # --retry 5 is a safeguard
  local all_modules=$(curl --retry 5 -s ${KDE_PROJECTS_API_URL}/identifiers?active=true | jq -r '.[]' 2>/dev/null)
  if [ $? -ne 0 ]; then
    all_modules=""
  fi
  modules=""
  # most of the code which follows should and wll be replaced
  # by proper filtering on the API side
  for M in ${all_modules}; do
    local project_details=$(curl --retry 5 -s ${KDE_PROJECTS_API_URL}/identifier/${M} 2>/dev/null)
    local found_repo=$(echo "${project_details}" | jq -r '.repo' 2>/dev/null)

    [[ "${found_repo}" =~ (unmaintained|historical|sysadmin)/ ]] && continue

    local found_branch=$(echo "${project_details}" | jq -r ".i18n.${I18N_BRANCH}" 2>/dev/null)
    if [ -z "${found_branch}" ] || [ "${found_branch}" = "null" ]; then
      found_branch="none"
    fi

    expected_branch=$(get_branch ${M})
    if [ -z "${expected_branch}" ] || [ "${expected_branch}" = "get_branch_none" ]; then
      expected_branch="none"
    fi
    if [ "${found_branch}" != "${expected_branch}" ]; then
      printf "Warning: '%s' has different branches in get_paths (%s) than in XML file (%s)\n" ${M} ${expected_branch} ${found_branch} >&2
    fi
    if [ "${expected_branch}" != "none" ]; then
      modules="${modules} ${M}"
    fi
  done
  echo $modules
}

function get_path
{
# kde-dev-scripts, kconfigwidgets and kdoctools are not
# part of stable kf5, just here for update_translations needs
	case "$1" in
		kde-dev-scripts)
			echo git-stable/$1
			;;
		kconfigwidgets|kdoctools)
                        echo git-unstable-kf5/$1
                        ;;
		l10n)
			echo branches/stable/l10n-kf5
			;;
		*)
                        echo git-stable-kf5/$1
			;;
	esac
}

function get_po_path
{
	echo $1
}

function get_vcs
{
	case "$1" in
		l10n)
			echo svn
			;;
		*)
			echo git
			;;
	esac
}

function get_branch
{
	case "$1" in
		phonon-vlc)
			echo "0.11"
			;;
		phonon-gstreamer)
			echo "4.10"
			;;
		phonon)
			echo "4.11"
			;;
		kdialog|keditbookmarks|kfind|konqueror|kate|khelpcenter|konsole|yakuake|dolphin|analitza|artikulate|blinken|cantor|kalgebra|kalzium|kanagram|kbruch|kdeedu-data|kgeography|khangman|kig|kiten|klettres|kmplot|kqtquickcharts|ktouch|kturtle|kwordquiz|libkeduvocdocument|marble|minuet|parley|rocs|step|colord-kde|gwenview|kamera|kcolorchooser|kdegraphics-mobipocket|kdegraphics-thumbnailers|kolourpaint|kruler|spectacle|libkdcraw|libkexiv2|libkipi|libksane|okular|svgpart|ark|filelight|kcalc|kcharselect|kdebugsettings|kdf|kfloppy|kgpg|ktimer|kwalletmanager|markdownpart|print-manager|sweeper|kmag|kmousetool|kmouth|kontrast|audiocd-kio|dragon|ffmpegthumbs|juk|k3b|kdenlive|kmix|kwave|libkcddb|libkcompactdisc|bomber|bovo|granatier|kajongg|kapman|katomic|kblackbox|kblocks|kbounce|kbreakout|kdiamond|kfourinline|kgoldrunner|kigo|killbots|kiriki|kjumpingcube|klickety|klines|kmahjongg|kmines|knavalbattle|knetwalk|knights|kolf|kollision|konquest|kpat|kreversi|kshisen|ksirk|ksnakeduel|kspaceduel|ksquares|ksudoku|ktuberling|kubrick|libkdegames|libkmahjongg|lskat|palapeli|picmi|cervisia|dolphin-plugins|kapptemplate|kcachegrind|kde-dev-utils|kde-dev-scripts|kdesdk-kio|kdesdk-thumbnailers|kompare|libkomparediff2|lokalize|poxml|umbrello|kteatime|kcron|kpmcore|ksystemlog|partitionmanager|kdenetwork-filesharing|kget|kio-extras|kio-gdrive|kio-zeroconf|konversation|kopete|krdc|krfb|ktorrent|ktp-accounts-kcm|ktp-approver|ktp-auth-handler|ktp-call-ui|ktp-common-internals|ktp-contact-list|ktp-contact-runner|ktp-desktop-applets|ktp-filetransfer-handler|ktp-kded-module|ktp-send-file|ktp-text-ui|libktorrent|kaccounts-integration|kaccounts-providers|signon-kwallet-extension|kross-interpreters|baloo-widgets|kosmindoormap|kpublictransport|akonadi|akonadi-search|kcalutils|kidentitymanagement|kimap|kldap|kleopatra|kmailtransport|kmbox|kmime|kontactinterface|kpimtextedit|ktnef|akonadi-calendar|akonadi-contacts|akonadi-notes|akonadi-mime|akonadi-calendar-tools|akonadiconsole|akonadi-import-wizard|akregator|grantlee-editor|kaddressbook|kalarm|kmail|kmail-account-wizard|knotes|kontact|korganizer|mbox-importer|pim-data-exporter|pim-sieve-editor|kdepim-runtime|kdepim-addons|calendarsupport|eventviews|grantleetheme|incidenceeditor|itinerary|libgravatar|libkdepim|libkgapi|libkleo|libksieve|mailcommon|mailimporter|messagelib|pimcommon|kpkpass|kitinerary|ksmtp|kimagemapeditor|kbackup|kamoso|kirigami-gallery|kipi-plugins|elisa|kdeconnect-kde|kopeninghours|ksanecore|skanlite|skanpage|zanshin|kdevelop|kdev-php|kdev-python|falkon|kalendar|alligator|angelfish|audiotube|calindori|ghostwriter|kalk|kasts|kclock|kde-inotify-survey|keysmith|kio-admin|kjournald|koko|kongress|krecorder|ktrip|kweather|neochat|plasmatube|qmlkonsole|telly-skout|tokodon)
			# Gear (formerly Release Service)
			echo "release/23.04"
 			;;
		wacomtablet)
			echo "3.3"
			;;
		kdiagram)
			echo "2.8"
			;;
		krita)
			echo "krita/5.1"
			;;
		calligra)
			echo "calligra/3.3"
			;;
		calligraplan)
			echo "3.3"
			;;
		gcompris)
			echo "KDE/3.0"
			;;
		kdiff3)
			echo "1.10"
			;;
		krename)
			echo "5.0"
			;;
		kronometer)
			echo "2.3"
			;;
		krusader)
			echo "stable"
			;;
		okteta)
			echo "0.26"
			;;
		symboleditor)
			echo "release-2.1.0"
			;;
		kxstitch)
			echo "release-2.2.0"
			;;
		atcore)
			echo "1.0"
			;;
		latte-dock|liblatte)
			echo "v0.10"
			;;
		qtcurve)
			echo "1.9"
			;;
		kdevelop-pg-qt)
			echo "2.2"
			;;
		kmplayer)
			echo "0.12"
			;;
		smb4k)
			echo "3.2"
			;;
		kexi|kdb|kproperty|kreport)
			echo "3.2"
			;;
		alkimia)
			echo "8.1"
			;;
		kbibtex)
			echo "kbibtex/0.10"
			;;
		kmymoney)
			echo "5.1"
			;;
		tellico)
			echo "3.4"
			;;
		kdesvn)
			echo "2.1"
			;;
		massif-visualizer)
			echo "0.7"
			;;
		heaptrack)
			echo "1.4"
			;;
		ikona)
			echo "1.0"
			;;
		ruqola)
			echo "1.9"
			;;
		symmy)
			echo "release/1.0"
			;;
		labplot)
			echo "release/2.9"
			;;
		kio-s3)
			echo "1.0"
			;;
		ktextaddons)
			echo "1.1"
			;;
		aura-browser|bluedevil|breeze|breeze-grub|breeze-gtk|breeze-plymouth|discover|drkonqi|kactivitymanagerd|kde-cli-tools|kdecoration|kde-gtk-config|kdeplasma-addons|kgamma5|khotkeys|kinfocenter|kmenuedit|kpipewire|kscreen|kscreenlocker|ksshaskpass|ksystemstats|kwallet-pam|kwayland-integration|kwin|kwrited|libkscreen|libksysguard|milou|oxygen|plank-player|plasma-bigscreen|plasma-browser-integration|plasma-desktop|plasma-disks|plasma-integration|plasma-mobile|plasma-nano|plasma-nm|plasma-pa|plasma-remotecontrollers|plasma-sdk|plasma-systemmonitor|plasma-tests|plasma-thunderbolt|plasma-vault|plasma-workspace|plasma-workspace-wallpapers|plymouth-kcm|polkit-kde-agent-1|powerdevil|sddm-kcm|systemsettings|xdg-desktop-portal-kde|qqc2-breeze-style|plasma-firewall|layer-shell-qt|flatpak-kcm|plasma-welcome)
			echo "Plasma/5.27"
 			;;
		*)
			echo "get_branch_none"
			exit 1
			;;
	esac
}

function get_repo_name
{
	echo $(get_po_path $1)
}

function get_full_repo_path
{
	check_kde_projects_requirements

	local repo_full_path=$(curl --retry 5 -s ${KDE_PROJECTS_API_URL}/identifier/$1 | jq -r '.repo' 2>/dev/null)
	res=$?

	if [ ${res} -eq 0 ] && [ -n "${repo_full_path}" ]; then
		echo "${repo_full_path}"
	else
		echo "ERROR: url not found for $1"
		exit 1
	fi
}

function get_url
{
	if [ -n "$1" ]; then
		echo "kde:$(get_full_repo_path $1).git"
	fi 
}
