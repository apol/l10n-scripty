# Rule file for the process_orphans.sh script
# Comments must start with the # character at first column with a space after it
# Empty lines are allowed
# Format: command origin [destination] [revision] [date]
# Available commands: move copy delete merge mergekeep
# Use only *one* space to separate data
# Note: origin and destination must be PO files, not POT files

move docmessages/kpackage/kpackagetool_man-kpackagetool5.1.po docmessages/kpackage/kpackagetool_man-kpackagetool6.1.po
delete docmessages/kservice/desktoptojson_man-desktoptojson.8.po

move messages/kinfocenter/kcmsamba.po messages/kinfocenter/kcm_samba.po
move messages/kgamma5/kgamma5._desktop_.po messages/kgamma5/kgamma5._json_.po
move messages/kcoreaddons/kde5_xml_mimetypes.po messages/kcoreaddons/kde6_xml_mimetypes.po

delete docmessages/kconfigwidgets/preparetips5_man-preparetips5.1.po

delete messages/kcmutils/kcmutils._desktop_.po
delete messages/kpackage/kpackage._desktop_.po
delete messages/plasma-pa/plasma-pa._desktop_.po
delete messages/plasma-vault/plasma-vault._desktop_.po

move messages/plasma-mobile/plasma_applet_org.kde.phone.homescreen.po messages/plasma-mobile/plasma_applet_org.kde.plasma.mobile.homescreen.po
move messages/plasma-mobile/plasma_applet_org.kde.phone.homescreen.simple.po messages/plasma-mobile/plasma_applet_org.kde.plasma.mobile.homescreen.halcyon.po

copy messages/plasma-sdk/plasma-sdk._desktop_.po messages/plasma-sdk/plasma-sdk._json_.po

move messages/karchive/karchive5_qt.po messages/karchive/karchive6_qt.po
move messages/kcodecs/kcodecs5_qt.po messages/kcodecs/kcodecs6_qt.po
move messages/kconfig/kconfig5_qt.po messages/kconfig/kconfig6_qt.po
move messages/kcoreaddons/kcoreaddons5_qt.po messages/kcoreaddons/kcoreaddons6_qt.po
move messages/kdbusaddons/kdbusaddons5_qt.po messages/kdbusaddons/kdbusaddons6_qt.po
move messages/kdnssd/kdnssd5_qt.po messages/kdnssd/kdnssd6_qt.po
move messages/kholidays/libkholidays5_qt.po messages/kholidays/libkholidays6_qt.po
move messages/ki18n/ki18n5.po messages/ki18n/ki18n6.po
move messages/kitemviews/kitemviews5_qt.po messages/kitemviews/kitemviews6_qt.po
move messages/kwidgetsaddons/kwidgetsaddons5_qt.po messages/kwidgetsaddons/kwidgetsaddons6_qt.po
move messages/kwindowsystem/kwindowsystem5_qt.po messages/kwindowsystem/kwindowsystem6_qt.po
move messages/solid/solid5_qt.po messages/solid/solid6_qt.po
move messages/sonnet/sonnet5_qt.po messages/sonnet/sonnet6_qt.po
move messages/syntax-highlighting/syntaxhighlighting5_qt.po messages/syntax-highlighting/syntaxhighlighting6_qt.po

move messages/kauth/kauth5_qt.po messages/kauth/kauth6_qt.po
move messages/kcompletion/kcompletion5_qt.po messages/kcompletion/kcompletion6_qt.po
move messages/kcontacts/kcontacts5.po messages/kcontacts/kcontacts6.po
move messages/knotifications/knotifications5_qt.po messages/knotifications/knotifications6_qt.po
move messages/kfilemetadata/kfilemetadata5.po messages/kfilemetadata/kfilemetadata6.po

move messages/kjobwidgets/kjobwidgets5_qt.po messages/kjobwidgets/kjobwidgets6_qt.po
move messages/kpackage/libkpackage5.po messages/kpackage/libkpackage6.po
move messages/kpeople/kpeople5.po messages/kpeople/kpeople6.po
move messages/kpty/kpty5.po messages/kpty/kpty6.po
move messages/kunitconversion/kunitconversion5.po messages/kunitconversion/kunitconversion6.po
move messages/kcmutils/kcmutils5.po messages/kcmutils/kcmutils6.po
move messages/kbookmarks/kbookmarks5_qt.po messages/kbookmarks/kbookmarks6_qt.po
move messages/kconfigwidgets/kconfigwidgets5.po messages/kconfigwidgets/kconfigwidgets6.po
move messages/kdav/libkdav.po messages/kdav/libkdav6.po
move messages/kglobalaccel/kglobalaccel5_qt.po messages/kglobalaccel/kglobalaccel6_qt.po

move messages/baloo/balooctl5.po messages/baloo/balooctl6.po
move messages/baloo/baloodb5.po messages/baloo/baloodb6.po
move messages/baloo/balooengine5.po messages/baloo/balooengine6.po
move messages/baloo/baloo_file5.po messages/baloo/baloo_file6.po
move messages/baloo/baloo_file_extractor5.po messages/baloo/baloo_file_extractor6.po
move messages/baloo/baloosearch5.po messages/baloo/baloosearch6.po
move messages/baloo/balooshow5.po messages/baloo/balooshow6.po
move messages/baloo/kio5_baloosearch.po messages/baloo/kio6_baloosearch.po
move messages/baloo/kio5_tags.po messages/baloo/kio6_tags.po
move messages/baloo/kio5_timeline.po messages/baloo/kio6_timeline.po
move messages/kdesu/kdesud5.po messages/kdesu/kdesud6.po
move messages/kiconthemes/kiconthemes5.po messages/kiconthemes/kiconthemes6.po
move messages/knewstuff/knewstuff5.po messages/knewstuff/knewstuff6.po
move messages/knotifyconfig/knotifyconfig5.po messages/knotifyconfig/knotifyconfig6.po
move messages/kparts/kparts5.po messages/kparts/kparts6.po
move messages/kservice/kservice5.po messages/kservice/kservice6.po

move messages/kirigami/libkirigami2plugin_qt.po messages/kirigami/libkirigami6_qt.po
move messages/kdeclarative/kdeclarative5.po messages/kdeclarative/kdeclarative6.po
move messages/ktexteditor/ktexteditor5.po messages/ktexteditor/ktexteditor6.po
move messages/ktextwidgets/ktextwidgets5.po messages/ktextwidgets/ktextwidgets6.po
move messages/kwallet/kwalletd5.po messages/kwallet/kwalletd6.po
move messages/kxmlgui/kxmlgui5.po messages/kxmlgui/kxmlgui6.po
move messages/plasma-framework/libplasma5.po messages/plasma-framework/libplasma6.po

move messages/plasma-disks/plasma_disks.po messages/plasma-disks/kcm_disks.po
