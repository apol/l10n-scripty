# Rule file for the process_orphans.sh script
# Comments must start with the # character at first column with a space after it
# Empty lines are allowed
# Format: command origin [destination] [revision] [date]
# Available commands: move copy delete merge mergekeep
# Use only *one* space to separate data
# Note: origin and destination must be PO files, not POT files

move messages/calindori/org.kde.phone.calindori.appdata.po messages/calindori/org.kde.calindori.appdata.po
delete messages/kdevelop/kdevkdeprovider.po
delete messages/kdepim-apps-libs/libsendlater.po

delete messages/kig/kfile_drgeo.po
delete messages/kig/kfile_kig.po

move messages/alligator/org.kde.mobile.alligator.appdata.po messages/alligator/org.kde.alligator.appdata.po

# leftover of the flattening
move messages/kdelibs4support/l10n._desktop_.po messages/kconfigwidgets/l10n._desktop_.po

move messages/maui-dialer/maui-dialer._desktop_.po messages/maui-communicator/maui-communicator._desktop_.po

move messages/kscreenlocker/screenlocker_kcm.po messages/kscreenlocker/kcm_screenlocker.po

move messages/kcoreaddons/xml_mimetypes5.po messages/kcoreaddons/kde5_xml_mimetypes.po

delete docmessages/kde-dev-scripts/scripts_man-reportview.1.po
delete docmessages/kde-dev-scripts/scripts_man-transxx.1.po

move messages/plasma-camera/org.kde.mobile.camera.appdata.po messages/plasma-camera/org.kde.plasma.camera.appdata.po

move messages/kpat/kpat_mimetypes.po messages/kpat/kpat_xml_mimetypes.po

move messages/plasma-desktop/kfontinst.po messages/plasma-workspace/kfontinst.po
move messages/plasma-desktop/krdb.po messages/plasma-workspace/krdb.po
move messages/plasma-desktop/kcm_colors.po messages/plasma-workspace/kcm_colors.po
move messages/plasma-desktop/kcm_cursortheme.po messages/plasma-workspace/kcm_cursortheme.po
move messages/plasma-desktop/kcm_fonts.po messages/plasma-workspace/kcm_fonts.po
move messages/plasma-desktop/kcm_lookandfeel.po messages/plasma-workspace/kcm_lookandfeel.po
move messages/plasma-desktop/kcm_style.po messages/plasma-workspace/kcm_style.po
move docmessages/plasma-desktop/kcontrol_colors.po docmessages/plasma-workspace/kcontrol_colors.po
move docmessages/plasma-desktop/kcontrol_fontinst.po docmessages/plasma-workspace/kcontrol_fontinst.po
move docmessages/plasma-desktop/kcontrol_fonts.po docmessages/plasma-workspace/kcontrol_fonts.po
move docmessages/plasma-desktop/kcontrol_kcmstyle.po docmessages/plasma-workspace/kcontrol_kcmstyle.po

delete messages/ruqola/org.kde.ruqolaqml.appdata.po

merge messages/kdepim-apps-libs/libkaddressbookimportexport.po messages/kaddressbook/kaddressbook.po
delete messages/kdepim-apps-libs/libkaddressbookimportexport.po

move messages/kinfocenter/kcmnic.po messages/kinfocenter/kcm_nic.po

move messages/websites-kde-org-announcements-releases/kde-org-announcements-releases.po messages/websites-kde-org/release_announcements.po

move messages/kpmcore/kpmcore._desktop_.po messages/kpmcore/kpmcore._policy_.po

copy messages/cutehmi/cutehmi_qt.po messages/cutehmi/cutehmi-2_qt.po
copy messages/cutehmi/cutehmi_qt.po messages/cutehmi/cutehmi-view-4_qt.po
delete messages/cutehmi/cutehmi_qt.po

delete messages/websites-docs-krita-org/docs_krita_org_reference_manual___dockers___snap_settings_docker.po
delete messages/websites-docs-krita-org/docs_krita_org_reference_manual___preferences___grid_settings.po
delete messages/websites-docs-krita-org/docs_krita_org_reference_manual___tools___grid_tool.po
delete messages/websites-docs-krita-org/docs_krita_org_reference_manual___tools___perspective_grid.po

move messages/arkade/org.kde.gamecenter.appdata.po messages/arkade/org.kde.arkade.appdata.po

move messages/totalreqall/TotalReqall.po messages/totalreqall/totalreqall.po

move messages/mygnuhealth/org.gnuhealth.my.metainfo.po messages/mygnuhealth/org.kde.mygnuhealth.metainfo.po

delete messages/plasma-browser-integration/plasma_runner_browsertabs.po

move messages/plasma-desktop/kcmsmserver.po messages/plasma-desktop/kcm_smserver.po

move messages/plasma-desktop/kcm5_componentchooser.po messages/plasma-desktop/kcm_componentchooser.po

move messages/plasma-angelfish/org.kde.mobile.angelfish.appdata.po messages/plasma-angelfish/org.kde.mobile.angelfish.metainfo.po
move messages/plasma-phonebook/org.kde.phonebook.appdata.po messages/plasma-phonebook/org.kde.phonebook.metainfo.po

move messages/maui-cinema/cinema.po messages/maui-clip/clip.po
move messages/maui-cinema/maui-cinema._desktop_.po messages/maui-clip/maui-clip._desktop_.po

move messages/websites-kde-org/menu_footer_shared.po messages/websites-aether-sass/aether-sass-shared-translations.po

copy messages/websites-kde-org/www_www.po messages/websites-kde-org/frameworks_announcements.po

move messages/angelfish/plasma-angelfish._desktop_.po messages/angelfish/angelfish._desktop_.po

move messages/calindori/calindori_qt.po messages/calindori/calindori.po

copy messages/ksysguard/ksysguard_plugins_global.po messages/ksystemstats/ksystemstats_plugins.po

move messages/plasma-desktop/kcm_autostart.po messages/plasma-workspace/kcm_autostart.po
move docmessages/plasma-desktop/kcontrol_autostart.po docmessages/plasma-workspace/kcontrol_autostart.po
move messages/ksysguard/ksysguard_plugins_process.po messages/libksysguard/ksysguard_plugins_process.po
delete messages/ksysguard/ksysguard_plugins_global.po

move messages/plasma-desktop/kcmformats.po messages/plasma-workspace/kcmformats.po
move messages/plasma-desktop/kcm_nightcolor.po messages/plasma-workspace/kcm_nightcolor.po
move messages/plasma-desktop/kcm_notifications.po messages/plasma-workspace/kcm_notifications.po
move docmessages/plasma-desktop/kcontrol_formats.po docmessages/plasma-workspace/kcontrol_formats.po
move docmessages/plasma-desktop/kcontrol_notifications.po docmessages/plasma-workspace/kcontrol_notifications.po

move messages/angelfish/org.kde.mobile.angelfish.metainfo.po messages/angelfish/org.kde.angelfish.metainfo.po

delete messages/kalgebra/kalgebra._json_.po
delete messages/ksysguard/ksysguard._json_.po

copy messages/zeroconf-ioslave/zeroconf-ioslave._desktop_.po messages/zeroconf-ioslave/zeroconf-ioslave._json_.po

copy messages/k3b/k3b._desktop_.po messages/k3b/k3b._json_.po

move messages/kjournald/org.kde.journald-browser.appdata.po messages/kjournald/org.kde.kjournaldbrowser.appdata.po

delete messages/websites-docs-krita-org/docs_krita_org_reference_manual___dockers___animation_curve.po
delete messages/websites-docs-krita-org/docs_krita_org_reference_manual___dockers___timeline.po
delete messages/websites-docs-krita-org/docs_krita_org_reference_manual___tools___color_selector.po
delete messages/websites-docs-krita-org/docs_krita_org_reference_manual___tools___outline_select.po

move messages/kinfocenter/kcm-about-distro.po messages/kinfocenter/kcm_about-distro.po

delete messages/kinfocenter/kcmopengl.po

move messages/plasma-desktop/kcmaccess.po messages/plasma-desktop/kcm_access.po

delete messages/documentation-docs-kdenlive-org/docs_kdenlive_org_effects_and_compositions___effect_groups___alpha_operation_transitions.po
delete messages/documentation-docs-kdenlive-org/docs_kdenlive_org_effects_and_compositions___effect_groups___audio_correction___volume_(keyframable).po
delete messages/documentation-docs-kdenlive-org/docs_kdenlive_org_effects_and_compositions___effect_groups___colour_correction___brightness_(keyframable).po
delete messages/documentation-docs-kdenlive-org/docs_kdenlive_org_effects_and_compositions___effect_groups___colour_correction___white_balance(lms).po
delete messages/documentation-docs-kdenlive-org/docs_kdenlive_org_effects_and_compositions___effect_groups___crop_and_transform___rotate_(keyframable).po
delete messages/documentation-docs-kdenlive-org/docs_kdenlive_org_effects_and_compositions___effect_groups.po
delete messages/documentation-docs-kdenlive-org/docs_kdenlive_org_glossary___useful_information___configuring_the_default_transition_duration.po
delete messages/documentation-docs-kdenlive-org/docs_kdenlive_org_glossary___useful_information___color_hell_ffmpeg_transcoding_and_preserving_BT.601.po
delete messages/documentation-docs-kdenlive-org/docs_kdenlive_org_glossary___useful_information___fixing_unwanted__slow_audio_fade-ins_with_some_USB_audio_cards.po
delete messages/documentation-docs-kdenlive-org/docs_kdenlive_org_glossary___useful_information___the_smooth_keyframe_interpolation.po

move messages/plasma-workspace/kcm5_icons.po messages/plasma-workspace/kcm_icons.po

copy messages/sddm-kcm/sddm-kcm._desktop_.po messages/sddm-kcm/sddm-kcm._json_.po
copy messages/plymouth-kcm/plymouth-kcm._desktop_.po messages/plymouth-kcm/plymouth-kcm._json_.po
delete messages/print-manager/print-manager._json_.po
copy messages/print-manager/print-manager._desktop_.po messages/print-manager/print-manager._json_.po
copy messages/discover/discover._desktop_.po messages/discover/discover._json_.po

copy messages/kamera/kamera._desktop_.po messages/kamera/kamera._json_.po

copy messages/plasma-desktop/plasma-desktop._desktop_.po messages/plasma-desktop/plasma-desktop._json_.po

delete messages/plasma-workspace/plasma-workspace._json_.po
copy messages/plasma-workspace/plasma-workspace._desktop_.po messages/plasma-workspace/plasma-workspace._json_.po

copy messages/knotes/knotes._desktop_.po messages/knotes/knotes._json_.po
copy messages/korganizer/korganizer._desktop_.po messages/korganizer/korganizer._json_.po
copy messages/kmail/kmail._desktop_.po messages/kmail/kmail._json_.po
copy messages/akregator/akregator._desktop_.po messages/akregator/akregator._json_.po

move messages/kget/kgetplugin.po messages/konqueror/kgetplugin.po

copy messages/plasma-thunderbolt/plasma-thunderbolt._desktop_.po messages/plasma-thunderbolt/plasma-thunderbolt._json_.po
copy messages/ktorrent/ktorrent._desktop_.po messages/ktorrent/ktorrent._json_.po

move messages/kde-cli-tools/kioclient5.po messages/kde-cli-tools/kioclient.po

# not handled by orphans: plasma-phone-components directory renamed as plasma-mobile
move messages/plasma-mobile/plasma-phone-components._desktop_.po messages/plasma-mobile/plasma-mobile._desktop_.po

copy messages/kde-cli-tools/kde-cli-tools._desktop_.po messages/kde-cli-tools/kde-cli-tools._json_.po
copy messages/plasma-nm/plasma-nm._desktop_.po messages/plasma-nm/plasma-nm._json_.po
copy messages/plasma-settings/plasma-settings._desktop_.po messages/plasma-settings/plasma-settings._json_.po

move messages/kdeconnect-kde/org.kde.kdeconnect.kcm.appdata.po messages/kdeconnect-kde/org.kde.kdeconnect.kcm.metainfo.po

move messages/khelpcenter/org.kde.Help.appdata.po messages/khelpcenter/org.kde.khelpcenter.metainfo.po

move messages/kalendar/kalendarac.po messages/akonadi-calendar/kalendarac.po

copy messages/dolphin/dolphin._desktop_.po messages/dolphin/dolphin._json_.po

copy messages/latte-dock/latte-dock._desktop_.po messages/latte-dock/latte-dock._json_.po

move docmessages/calligraplan/calligraplan_contexthelp.po docmessages/calligraplan/calligraplan_plan_contexthelp.po
move docmessages/calligraplan/calligraplan_costbreakdownstructureeditor.po docmessages/calligraplan/calligraplan_plan_costbreakdownstructureeditor.po
move docmessages/calligraplan/calligraplan_creatingodtreporttemplates.po docmessages/calligraplan/calligraplan_plan_creatingodtreporttemplates.po
move docmessages/calligraplan/calligraplan_creatingproject.po docmessages/calligraplan/calligraplan_plan_creatingproject.po
move docmessages/calligraplan/calligraplan_definitions.po docmessages/calligraplan/calligraplan_plan_definitions.po
move docmessages/calligraplan/calligraplan_guidelines.po docmessages/calligraplan/calligraplan_plan_guidelines.po
move docmessages/calligraplan/calligraplan_introduction.po docmessages/calligraplan/calligraplan_plan_introduction.po
move docmessages/calligraplan/calligraplan_mainworkspace.po docmessages/calligraplan/calligraplan_plan_mainworkspace.po
move docmessages/calligraplan/calligraplan_managingresources.po docmessages/calligraplan/calligraplan_plan_managingresources.po
move docmessages/calligraplan/calligraplan_milestoneganttview.po docmessages/calligraplan/calligraplan_plan_milestoneganttview.po
move docmessages/calligraplan/calligraplan_planintroduction.po docmessages/calligraplan/calligraplan_plan_planintroduction.po
move docmessages/calligraplan/calligraplan.po docmessages/calligraplan/calligraplan_plan.po
move docmessages/calligraplan/calligraplan_projectperformanceview.po docmessages/calligraplan/calligraplan_plan_projectperformanceview.po
move docmessages/calligraplan/calligraplan_reportsgeneratorview.po docmessages/calligraplan/calligraplan_plan_reportsgeneratorview.po
move docmessages/calligraplan/calligraplan_reports.po docmessages/calligraplan/calligraplan_plan_reports.po
move docmessages/calligraplan/calligraplan_resourceassignmentganttview.po docmessages/calligraplan/calligraplan_plan_resourceassignmentganttview.po
move docmessages/calligraplan/calligraplan_resourceassignmentview.po docmessages/calligraplan/calligraplan_plan_resourceassignmentview.po
move docmessages/calligraplan/calligraplan_resourceeditor.po docmessages/calligraplan/calligraplan_plan_resourceeditor.po
move docmessages/calligraplan/calligraplan_scheduleseditor.po docmessages/calligraplan/calligraplan_plan_scheduleseditor.po
move docmessages/calligraplan/calligraplan_startupview.po docmessages/calligraplan/calligraplan_plan_startupview.po
move docmessages/calligraplan/calligraplan_taskdependencyeditorgraphical.po docmessages/calligraplan/calligraplan_plan_taskdependencyeditorgraphical.po
move docmessages/calligraplan/calligraplan_taskdependencyeditorlist.po docmessages/calligraplan/calligraplan_plan_taskdependencyeditorlist.po
move docmessages/calligraplan/calligraplan_taskeditor.po docmessages/calligraplan/calligraplan_plan_taskeditor.po
move docmessages/calligraplan/calligraplan_taskexecutionview.po docmessages/calligraplan/calligraplan_plan_taskexecutionview.po
move docmessages/calligraplan/calligraplan_taskganttview.po docmessages/calligraplan/calligraplan_plan_taskganttview.po
move docmessages/calligraplan/calligraplan_taskperformanceview.po docmessages/calligraplan/calligraplan_plan_taskperformanceview.po
move docmessages/calligraplan/calligraplan_taskstatusview.po docmessages/calligraplan/calligraplan_plan_taskstatusview.po
move docmessages/calligraplan/calligraplan_usingtheviews.po docmessages/calligraplan/calligraplan_plan_usingtheviews.po
move docmessages/calligraplan/calligraplan_viewsandeditors.po docmessages/calligraplan/calligraplan_plan_viewsandeditors.po
move docmessages/calligraplan/calligraplan_workandvacationeditor.po docmessages/calligraplan/calligraplan_plan_workandvacationeditor.po

move docmessages/plasma-workspace/kcontrol_translations.po docmessages/plasma-workspace/kcontrol_region_language.po

copy messages/plasma-desktop/kcm_search.po messages/plasma-desktop/kcm_krunnersettings.po
move messages/plasma-desktop/kcm_search.po messages/plasma-desktop/kcm_plasmasearch.po

# the zeroconf-ioslave directories were manually renamed as kio-zeroconf,
# not tracked/handled by process_orphans
move messages/kio-zeroconf/zeroconf-ioslave._desktop_.po messages/kio-zeroconf/kio-zeroconf._desktop_.po
move messages/kio-zeroconf/zeroconf-ioslave._json_.po messages/kio-zeroconf/kio-zeroconf._json_.po
move messages/kio-zeroconf/org.kde.zeroconf-ioslave.metainfo.po messages/kio-zeroconf/org.kde.kio_zeroconf.metainfo.po

copy messages/plasma-mobile/plasma-mobile._desktop_.po messages/plasma-mobile/plasma-mobile._json_.po

move messages/plasma-workspace-wallpapers/plasma-workspace-wallpapers._desktop_.po messages/plasma-workspace-wallpapers/plasma-workspace-wallpapers._json_.po

copy messages/powerdevil/powerdevil._desktop_.po messages/powerdevil/powerdevil._json_.po

copy messages/plasma-pa/plasma-pa._desktop_.po messages/plasma-pa/plasma-pa._json_.po

move messages/qmlkonsole/org.kde.mobile.qmlkonsole.appdata.po messages/qmlkonsole/org.kde.qmlkonsole.appdata.po

move messages/plasma-welcome/welcome-app._desktop_.po messages/plasma-welcome/plasma-welcome._desktop_.po

move messages/kwin/kcm-kwin-scripts.po messages/kwin/kcm_kwin_scripts.po

move messages/plasma-desktop/kcm_recentFiles5.po messages/plasma-desktop/kcm_recentFiles.po

copy messages/skrooge/skrooge._desktop_.po messages/skrooge/skrooge._json_.po
copy messages/wacomtablet/wacomtablet._desktop_.po messages/wacomtablet/wacomtablet._json_.po

move messages/maui-fiery/maui-sol._desktop_.po messages/maui-fiery/maui-fiery._desktop_.po

move messages/maui-booth/org.maui.booth.appdata.po messages/maui-booth/org.kde.booth.appdata.po
# old content, copied from maui-index
delete messages/bonsai/org.maui.bonsai.appdata.po

delete messages/plasma-settings/kcm_password.po
delete docmessages/libksieve/kioslave5_sieve.po

copy messages/plasma-bigscreen/plasma-bigscreen._desktop_.po messages/plasma-bigscreen/plasma-bigscreen._json_.po

move messages/documentation-docs-kdenlive-org/docs_kdenlive_org_user_interface___menu___clip_menu___markers.po messages/documentation-docs-kdenlive-org/docs_kdenlive_org_user_interface___menu___clip_menu___menu_markers.po
move messages/digikam-doc/docs_digikam_org_asset_management___data_corruption.po messages/digikam-doc/docs_digikam_org_asset_management___data_protection.po
Move messages/digikam-doc/docs_digikam_org_post_processing___share_dlna.po messages/digikam-doc/docs_digikam_org_post_processing___media_server.po

delete messages/digikam-doc/docs_digikam_org_tag_manager.po
delete messages/digikam-doc/docs_digikam_org_tag_manager___tagmanager_overview.po

delete messages/umbrello/umbrello_kdevphp.po

copy messages/kup/kup._desktop_.po messages/kup/kup._json_.po

move messages/ruqola/libtextautocorrection.po messages/ktextaddons/libtextautocorrection.po
move messages/ruqola/libtextedittexttospeech.po messages/ktextaddons/libtextedittexttospeech.po
move messages/ruqola/libtexttranslator.po messages/ktextaddons/libtexttranslator.po
copy messages/kdepim-addons/kmail_editorgrammar_plugins.po messages/ktextaddons/libtextgrammarcheck.po

copy messages/kwalletmanager/kwalletmanager._desktop_.po messages/kwalletmanager/kwalletmanager._json_.po

delete messages/digikam-doc/docs_digikam_org_image_editor___color_management.po

delete docmessages/kio/kioslave5_mailto.po

copy messages/kscreen/kscreen.po messages/kscreen/kscreen_common.po

move docmessages/plasma-desktop/kcontrol_cursortheme.po docmessages/plasma-workspace/kcontrol_cursortheme.po
delete docmessages/plasma-desktop/kcontrol_kcmlaunchfeedback.po

move messages/libkcddb/libkcddb._desktop_.po messages/libkcddb/libkcddb._json_.po
copy messages/kmix/kmix._desktop_.po messages/kmix/kmix._json_.po
copy messages/plasma-browser-integration/plasma-browser-integration._desktop_.po messages/plasma-browser-integration/plasma-browser-integration._json_.po
copy messages/plasma-disks/plasma-disks._desktop_.po messages/plasma-disks/plasma-disks._json_.po

move messages/ffmpegthumbs/ffmpegthumbs._desktop_.po messages/ffmpegthumbs/ffmpegthumbs._json_.po

# archived and/or branch disabled
delete docmessages/ksysguard/ksysguard.po
delete messages/ksysguard/ksysguard._desktop_.po
delete messages/ksysguard/ksysguard.po
delete messages/ksysguard/org.kde.ksysguard.appdata.po
delete messages/plasma-active-window-control/plasma-active-window-control._desktop_.po
delete messages/plasma-redshift-control/plasma-redshift-control._desktop_.po

move messages/kdegraphics-thumbnailers/kdegraphics-thumbnailers._desktop_.po messages/kdegraphics-thumbnailers/kdegraphics-thumbnailers._json_.po

move messages/websites-aether-sass/aether-sass-shared-translations._static_.po messages/websites-hugo-kde/hugo-kde._static_.po

move messages/gitklient/gitklient._desktop_.po messages/kommit/kommit._desktop_.po
move messages/gitklient/gitklient._json_.po messages/kommit/kommit._json_.po
move messages/gitklient/gitklient.po messages/kommit/kommit.po
move messages/gitklient/org.kde.gitklient.appdata.po messages/kommit/org.kde.kommit.appdata.po
