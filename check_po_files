#! /bin/bash

# Copyright 2007 Nicolas GOUTTE <goutte@kde.org>
# License: GNU LGPL 2.0+

# This script checks the PO files with the help of Gettext's msgfmt tool

l10nscripts="$(readlink -f $(dirname $0))"
kf_suffix="5"
if [ -n "${SCRIPTY_I18N_BRANCH}" ]; then
    # get the last character, the space before -1 is important
    kf_suffix="${SCRIPTY_I18N_BRANCH: -1}"
fi

copy_plural_header() {
    plural_master="$1"
    backup_path="$2"
    find_args="$3"

    if test ! -f "$plural_master" ; then
        plural_master="$backup_path/$plural_master"
    fi

    if test ! -f "$plural_master" ; then
        echo "Warning: Could not find file $plural_master to grab the plural formula from."
    else
        find $find_args | xargs perl ${l10nscripts}/change-header.pl "$plural_master"
    fi
}

if test -f subdirs; then
    for lang in `cat subdirs`; do
        # Note: we do not use 'cd' here, to allow msgfmt to generate errors and warnings with paths containing the language code

        # KF<n>: copy plural headers from kcoreaddons<n>_qt.po to all other Qt-based catalogs
        copy_plural_header "$lang/messages/kcoreaddons/kcoreaddons${kf_suffix}_qt.po" "$PLURAL_MASTER_PATH" "$lang -name *_qt.po"

        # KF<n>: copy plural headers from ki18n<n>.po to all other KI18n-based catalogs
        copy_plural_header "$lang/messages/ki18n/ki18n${kf_suffix}.po" "$PLURAL_MASTER_PATH" "$lang -name *.po ! -name *_qt.po"

        # Fix the 'Language' header to be the language name
        find $lang -name \*.po | xargs -r sed -i -e 's,^"Language: [-A-Za-z_]*\\n","Language: '$lang'\\n",g'

        # Check all PO files with Gettext's msgfmt
        find $lang -name \*.po | ionice -c 2 -n 7 nice -n 30 parallel msgfmt {} --check-header -o /dev/null 2>&1 | sort

        # Find POT files (which should not be there)
        pot=`find $lang -name \*.pot`
        if test `echo "$pot"| wc --bytes` -gt 1; then
            echo "Warning: language $lang contains POT files!"
            echo $pot
            echo "--- end of list ---"
        fi
    done
    echo "*_qt.po files without X-Qt-Contexts: true"
    find `cat subdirs` -type f -name \*_qt.po -print | xargs grep -L "X-Qt-Contexts: true"

    perl ${l10nscripts}/check_desktop_files
else
    echo "ERROR: could not find the subdirs file! (Wrong directory?)"
fi
